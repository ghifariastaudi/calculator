function calcCircleArea(radian) {
    return 3.14 * radian * radian
}

module.exports = {
    calcCircleArea,
}
