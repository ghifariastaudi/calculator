// Require function will allow you to call the module
const readline = require("readline");

// initiate import file to index.js
const rectangleArea = require("./rectangle.js");
const triangleArea = require("./triangle.js");
const circleArea = require("./circle.js");
const fiboNumber = require("./fibonacci.js");
const factorialNumber = require("./factorial.js");

// Initiate the readline to use stdin and stdout as input and output
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

console.clear(); 
console.log("Which one do you want to calculate?");
console.log(`
1. Rectangle Area
2. Triangle Area
3. Circle Area
4. Fibonacci Number
5. Factorial Number
`);

function handleAnswer1() {
  console.clear();
  console.log("Calculate Rectangle Area");

  rl.question("Width: ", width => {
    rl.question("Length: ", length => {
      console.log(
        "Here's the result:",
        rectangleArea.calculateRectangleArea(width, length)
      );
      rl.close();
    })
  })
}

function handleAnswer2() {
  console.clear();
  console.log("Calculate Triangle Area");

  rl.question("Base: ", base => {
    rl.question("Height: ", height => {
      console.log(
        "Here's the result:",
        triangleArea.calculateTriangleArea(base, height)
      );
      rl.close();
    })
  })
}

function handleAnswer3() {
  console.clear();
  console.log("Calculate Circle Area");

  rl.question("radian: ", radian => {
    console.log(
      "Here's the result:",
      circleArea.calcCircleArea(radian)
    );
    rl.close();
  })
}

function handleAnswer4() {
  console.clear();
  console.log("Calculate Fibonacci Number");
  
  rl.question("number: ", number => {
    console.log(
      "Here's the result:",
      fiboNumber.calcFibonacciNumber(number)
    );
    rl.close();
  })
}

function handleAnswer5() {
  console.clear();
  console.log("Calculate Factorial Number");

  rl.question("number: ", number => {
    console.log(
      "Here's the result:",
      factorialNumber.calcFactorialNumber(number)
    );
    rl.close();
  })
}

rl.question("Answer: ", answer => {
  if (answer == 1) {
    handleAnswer1();
  } 
  else if (answer == 2) {
    handleAnswer2();
  } 
  else if (answer == 3) {
    handleAnswer3();
  } 
  else if (answer == 4){
    handleAnswer4();
  }
  else if (answer == 5){
    handleAnswer5();
  }
  else {
    console.log("Sorry, there's no option for that!");
    rl.close();
  }
});