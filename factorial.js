function calcFactorialNumber(number) {
    let answer = 1;
    if (number == 0 || number == 1) {
        return answer;
    } else {
        for (var i = number; i >= 1; i--) {
            answer = answer * i;
        }
        return answer;
    }
}

module.exports = {
    calcFactorialNumber,
}
