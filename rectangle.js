function calculateRectangleArea(width, length) {
    return width * length;
}
module.exports = {
    calculateRectangleArea,
}
