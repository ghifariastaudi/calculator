function calcFibonacciNumber(number) {
    let num1 = 0;
    let num2 = 1;
    var sum;
    for (let i = 0; i < number; i++) {
        sum = num1 + num2;
        num1 = num2;
        num2 = sum;
    }
    return num2;
}
module.exports = { 
    calcFibonacciNumber,
}